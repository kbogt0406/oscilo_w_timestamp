----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    22:14:15 10/11/2014
-- Design Name:
-- Module Name:    SPI_W_LD - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SPI is
	 GENERIC( NSLAVES : Integer:= 3;
         Cont_mode : std_logic :='0';
				 CLK_DIV : Integer:=100;
				 N_BITS 	: Integer:=24);
    Port ( CLK : in  STD_LOGIC;
           RST : in  STD_LOGIC;
           DATA : in  STD_LOGIC_VECTOR(N_BITS-1 DOWNTO 0);
           SEND : in  STD_LOGIC;
           FLANK : in  STD_LOGIC;
           DEVICE : in  UNSIGNED(NSLAVES-1 DOWNTO 0);
           LDAC : in  STD_LOGIC;
			  RLED : out STD_LOGIC;
           SCK : out  STD_LOGIC;
           SDATA : out  STD_LOGIC;
           LOADDAC : out  STD_LOGIC;
           SS : out  STD_LOGIC_VECTOR (NSLAVES-1 downto 0));
end SPI;

architecture Behavioral of SPI is

--Definiendo los estado a utilizar
type spi_state is (idle, sstate, dsend, fstate);

signal cstate, nstate : spi_state;

--Definiendo el registro del spi
type spi_record is
record
	data: std_logic_vector(N_BITS-1 DOWNTO 0);
	flank : std_logic;
	device : integer RANGE 0 TO NSLAVES -1;
	ldac : std_logic;
end record;

--Definiendo registro de inicio.
constant spi_rst : spi_record :=  (data => (others => '0'),
										flank => '0',
										device => 0,
									   ldac => '0'
										);

signal spi : spi_record; -- definiendo la se�al a utilizar

-- Definiendo otras se�ales de ayuda
signal cs : std_logic_vector(NSLAVES -1 downto 0); -- se�al para controlar el SS
signal sck_signal : std_logic;
signal ready : std_logic;




begin

process(clk, rst)
variable clk_counter : integer range 0 to CLK_DIV-1;
begin
  if rst='0' then
    cstate<=idle;
    sck_signal <='1';
  elsif rising_edge(clk) then
    cstate<=nstate;
    if clk_counter = CLK_DIV-1 then
      sck_signal<=not sck_signal;
      clk_counter:=0;
    else
      clk_counter:=clk_counter+1;
    end if;    
  end if;
end process;

process(cstate, SEND, bit_counter)
begin
  case cstate is
    when idle =>
      if SEND='1' then
        IF LDAC='1' then
          nstate<=sstate;
        else
          nstate<=dsend;
        end if;
      else
        nstate<=idle;
      end if;
    when sstate =>
      nstate<=dsend;
    when dsend =>
      if bit_counter = N_BITS then
        nstate<=finish;
      else
        nstate<=dsend;
      end if;
    when fstate=>
      if Cont_mode ='1' then
        nstate<=idle;
      elsif SEND='0' then
        nstate<=idle;
      else
        nstate<=fstate;
      end if;
  end case;
end process;

end Behavioral;
