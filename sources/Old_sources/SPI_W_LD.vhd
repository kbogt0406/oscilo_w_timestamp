----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    22:14:15 10/11/2014
-- Design Name:
-- Module Name:    SPI_W_LD - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SPI_W_LD is
	 GENERIC( NSLAVES : Integer:= 3;
				 CLK_DIV : Integer:=100;
				 N_BITS 	: Integer:=24);
    Port ( CLK : in  STD_LOGIC;
           RST : in  STD_LOGIC;
           DATA : in  STD_LOGIC_VECTOR(N_BITS-1 DOWNTO 0);
           SEND : in  STD_LOGIC;
           FLANK : in  STD_LOGIC;
           DEVICE : in  UNSIGNED(NSLAVES-1 DOWNTO 0);
           LDAC : in  STD_LOGIC;
			  RLED : out STD_LOGIC;
           SCK : out  STD_LOGIC;
           SDATA : out  STD_LOGIC;
           LOADDAC : out  STD_LOGIC;
           SS : out  STD_LOGIC_VECTOR (NSLAVES-1 downto 0));
end SPI_W_LD;

architecture Behavioral of SPI_W_LD is

--Definiendo los estado a utilizar
type spi_state is (idle, sstate, d_send,  finish);

signal state : spi_state;

--Definiendo el registro del spi
type spi_record is
record
	data: std_logic_vector(N_BITS-1 DOWNTO 0);
	flank : std_logic;
	device : integer RANGE 0 TO NSLAVES -1;
	ldac : std_logic;
end record;

--Definiendo registro de inicio.
constant spi_rst : spi_record :=  (data => (others => '0'),
										flank => '0',
										device => 0,
									   ldac => '0'
										);

signal spi : spi_record; -- definiendo la seï¿½al a utilizar

-- Definiendo otras seï¿½ales de ayuda
signal cs : std_logic_vector(NSLAVES -1 downto 0); -- seï¿½al para controlar el SS
signal sck_signal : std_logic;
signal ready : std_logic;



begin

process(clk, rst)
variable clk_counter : integer range 0 to CLK_DIV-1;
variable bit_counter : integer range 1 to N_BITS+1;
begin
	if rst='0' then
		cs<=(others => '1'); -- Todas las salidas en 1
		ready <='1'; -- Seï¿½al lista para recibir datos.
		state<=idle; -- Estado inicial Idle
		spi<=spi_rst;
		--SDATA<= 'z' --Alta impedancia en la salida SDATA
	elsif rising_edge(clk) then
		case state is
			when idle =>
				if send='1' then
					clk_counter:=0;
					state<=sstate;
					spi.data<=data;
					spi.device<=to_integer(device);
					spi.ldac<=ldac;
					spi.flank<=flank;
					ready<='0';
				else
					cs<=(others =>'1');
					sck_signal<='Z';
					spi<=spi_rst;
					ready<='1';
					state<=idle;
				end if;
			when sstate =>
				sck_signal<=spi.flank;
				if spi.ldac ='1' then
					LOADDAC<='1';
					if clk_counter = clk_div -1 then
						clk_counter:= 0;
						bit_counter:=1;
						state<=d_send;
					else
						clk_counter:=clk_counter+1;
					end if;
				else
					 state<=d_send;
					 clk_counter:=0;
					 bit_counter:=1;
				end if;
			when d_send =>
				cs(spi.device)<='0';
				if bit_counter <= N_BITS then
					if clk_counter = clk_div/2 then
						SDATA<=spi.data(N_BITS - bit_counter);
						clk_counter:=clk_counter+1;
						sck_signal<=not(sck_signal);
					elsif clk_counter = clk_div-1 then
						sck_signal<=not(sck_signal);
						clk_counter:=0;
						bit_counter:=bit_counter+1;
					else
						clk_counter:=clk_counter+1;
					end if;
				else
					cs<=(others=>'1');
					clk_counter:=0;
					sck_signal<=spi.flank;
					state<=finish;
				end if;
			when finish =>
				if spi.ldac='1' then
					if clk_counter = clk_div-1 then
						LOADDAC<='0';
						state<=idle;
						ready<='1';
					else
						clk_counter:=clk_counter+1;
					end if;
				else
					ready<='1';
					if send='0' then
              state<=idle;
          else
              state<=finish;
          end if;
				end if;
			when others => -- No debe entrar
				state<=idle;
				cs<=(others=>'1');
				sdata<='Z';
				sck_signal<='Z';
		end case;
	end if;
end process;

SS<=cs;
sck<=sck_signal;
RLED<=ready;
end Behavioral;
