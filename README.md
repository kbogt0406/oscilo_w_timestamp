#Oscilo with Timestamp:

**Requirements:**

1. Vivado 2018.2.1

2. LabView 2017 (For the VIs)

3. **ip-repo** *available in* [link to ip-repo](https://bitbucket.org/kbogt0406/ip_repo/src/master/)

4. **timestamp repository** *available in* [link to timestamp](https://bitbucket.org/kbogt0406/timestamp/src/master/)


#How to import the project

1. Create a blank RTL project (Do not specify sources) for Zedboard
1. go to ip-repo on "Settings/IP/Repository"  and add the following locations
    **ip-repo** *available in* [link to ip-repo](https://bitbucket.org/kbogt0406/ip_repo/src/master/)
    <!-- **timestamp/iprepo** *available in* [link to timetamp](https://bitbucket.org/kbogt0406/timestamp/src/master/) -->

1. Add VHDL sources in the **sources** folder (everything except old sources sorry :) )  *available in* [VHDL  sources folder]{../sources)
1. From [timestamp repo](https://bitbucket.org/kbogt0406/timestamp/src/master/) add the VHDL sources in the folders **FIFO_MGR**, **Timestamp_Sources** and **mstrigger**.
1. From IP Catalog, Select "RAM-based Shift Register" and click in **Customize IP** and configure it this way:
![RAM-based_sr](SCR/RBSR.png), *IMPORTANT: generate output products as GLOBAL*

1. Add Constrains *available in* [Constrains folder](../constr)
1. On TCL Console type: `source <path_to_project>/bd/Top.tcl`
1. Create VHDL wrapper and set as top.
1. Generate output products **USING GLOBAL SETTING**
1. (Optional:) Delete other Design Sources not used in the project.
1. Generate Bitstream
1. SDK files are available in [sdk](../sdk) you can import directly from eclipse.
1. Current version of VI in Vivado is *Oscilo_Histo_TS_W_Log_Cummulative_histo.vi*
