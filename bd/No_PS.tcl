
################################################################
# This is a generated script based on design: design_2
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_2_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# DAC_CTRL1v1, adc_controller, FIFOMGR, mstrigger, timer, timestamp

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xczu9eg-ffvb1156-2-e
   set_property BOARD_PART xilinx.com:zcu102:part0:3.2 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_2

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:clk_wiz:6.0\
xilinx.com:ip:util_ds_buf:2.1\
xilinx.com:ip:proc_sys_reset:5.0\
user.org:user:comblock:1.0\
xilinx.com:user:ADC_Decimator:1.0\
ictp.it:user:Hist_Gen_Block:1.0\
xilinx.com:user:AXI_Trigger_gen_m4:1.0\
xilinx.com:ip:xlconstant:1.1\
xilinx.com:ip:fifo_generator:13.2\
xilinx.com:ip:axi_dma:7.1\
xilinx.com:ip:util_vector_logic:2.0\
xilinx.com:user:tlast_gen:1.0\
xilinx.com:ip:xlconcat:2.1\
xilinx.com:ip:xlslice:1.0\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

##################################################################
# CHECK Modules
##################################################################
set bCheckModules 1
if { $bCheckModules == 1 } {
   set list_check_mods "\ 
DAC_CTRL1v1\
adc_controller\
FIFOMGR\
mstrigger\
timer\
timestamp\
"

   set list_mods_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following modules exist in the project's sources: $list_check_mods ."

   foreach mod_vlnv $list_check_mods {
      if { [can_resolve_reference $mod_vlnv] == 0 } {
         lappend list_mods_missing $mod_vlnv
      }
   }

   if { $list_mods_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following module(s) are not found in the project: $list_mods_missing" }
      common::send_msg_id "BD_TCL-008" "INFO" "Please add source files for the missing module(s) above."
      set bCheckIPsPassed 0
   }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: timestamp
proc create_hier_cell_timestamp { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_timestamp() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 31 -to 0 FIFOMGR_CTRL_REG
  create_bd_pin -dir I FIFO_AEMPTY
  create_bd_pin -dir I FIFO_AFULL
  create_bd_pin -dir O FIFO_CLR
  create_bd_pin -dir O -from 31 -to 0 FIFO_DATA
  create_bd_pin -dir I FIFO_EMPTY
  create_bd_pin -dir I FIFO_FULL
  create_bd_pin -dir O FIFO_WE
  create_bd_pin -dir I -from 31 -to 0 In0
  create_bd_pin -dir I -from 31 -to 0 In1
  create_bd_pin -dir I -from 31 -to 0 In2
  create_bd_pin -dir I -from 31 -to 0 In3
  create_bd_pin -dir I -from 31 -to 0 ctrl_reg
  create_bd_pin -dir I -from 15 -to 0 din
  create_bd_pin -dir I fclk
  create_bd_pin -dir I -type rst rst
  create_bd_pin -dir I sclk
  create_bd_pin -dir I -from 31 -to 0 threshold
  create_bd_pin -dir O -from 31 -to 0 trigger
  create_bd_pin -dir O -from 31 -to 0 tsf
  create_bd_pin -dir O -from 31 -to 0 tss

  # Create instance: FIFOMGR_0, and set properties
  set block_name FIFOMGR
  set block_cell_name FIFOMGR_0
  if { [catch {set FIFOMGR_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $FIFOMGR_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: ctrl_reg, and set properties
  set ctrl_reg [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 ctrl_reg ]

  # Create instance: ctrl_reg1, and set properties
  set ctrl_reg1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 ctrl_reg1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DOUT_WIDTH {1} \
 ] $ctrl_reg1

  # Create instance: ctrl_reg2, and set properties
  set ctrl_reg2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 ctrl_reg2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {7} \
   CONFIG.DIN_TO {0} \
   CONFIG.DOUT_WIDTH {8} \
 ] $ctrl_reg2

  # Create instance: mstrigger_0, and set properties
  set block_name mstrigger
  set block_cell_name mstrigger_0
  if { [catch {set mstrigger_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $mstrigger_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: timer_0, and set properties
  set block_name timer
  set block_cell_name timer_0
  if { [catch {set timer_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $timer_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: timestamp_1, and set properties
  set block_name timestamp
  set block_cell_name timestamp_1
  if { [catch {set timestamp_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $timestamp_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]

  # Create instance: xlconcat_1, and set properties
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_1 ]

  # Create port connections
  connect_bd_net -net FIFOMGR_0_FIFO_CLR [get_bd_pins FIFO_CLR] [get_bd_pins FIFOMGR_0/FIFO_CLR]
  connect_bd_net -net FIFOMGR_0_FIFO_DATA [get_bd_pins FIFO_DATA] [get_bd_pins FIFOMGR_0/FIFO_DATA]
  connect_bd_net -net FIFOMGR_0_FIFO_WE [get_bd_pins FIFO_WE] [get_bd_pins FIFOMGR_0/FIFO_WE]
  connect_bd_net -net FIFOMGR_CTRL_REG_1 [get_bd_pins FIFOMGR_CTRL_REG] [get_bd_pins FIFOMGR_0/CTRL_REG]
  connect_bd_net -net FIFO_AEMPTY_1 [get_bd_pins FIFO_AEMPTY] [get_bd_pins FIFOMGR_0/FIFO_AEMPTY]
  connect_bd_net -net FIFO_AFULL_1 [get_bd_pins FIFO_AFULL] [get_bd_pins FIFOMGR_0/FIFO_AFULL]
  connect_bd_net -net FIFO_EMPTY_1 [get_bd_pins FIFO_EMPTY] [get_bd_pins FIFOMGR_0/FIFO_EMPTY]
  connect_bd_net -net FIFO_FULL_1 [get_bd_pins FIFO_FULL] [get_bd_pins FIFOMGR_0/FIFO_FULL]
  connect_bd_net -net In0_1 [get_bd_pins In0] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net In1_1 [get_bd_pins In1] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net In2_1 [get_bd_pins In2] [get_bd_pins xlconcat_1/In0]
  connect_bd_net -net In3_1 [get_bd_pins In3] [get_bd_pins xlconcat_1/In1]
  connect_bd_net -net Net [get_bd_pins rst] [get_bd_pins FIFOMGR_0/RST] [get_bd_pins mstrigger_0/rst] [get_bd_pins timer_0/rst] [get_bd_pins timestamp_1/rst]
  connect_bd_net -net ctrl_reg1_Dout [get_bd_pins ctrl_reg1/Dout] [get_bd_pins timestamp_1/clear]
  connect_bd_net -net ctrl_reg2_Dout [get_bd_pins ctrl_reg2/Dout] [get_bd_pins mstrigger_0/threshold]
  connect_bd_net -net ctrl_reg_1 [get_bd_pins ctrl_reg] [get_bd_pins ctrl_reg/Din] [get_bd_pins ctrl_reg1/Din]
  connect_bd_net -net din_1 [get_bd_pins din] [get_bd_pins FIFOMGR_0/ADC_DIN] [get_bd_pins mstrigger_0/din]
  connect_bd_net -net fclk_1 [get_bd_pins fclk] [get_bd_pins FIFOMGR_0/CLK] [get_bd_pins mstrigger_0/clk] [get_bd_pins timer_0/fclk] [get_bd_pins timestamp_1/fclk]
  connect_bd_net -net mstrigger_0_LAT [get_bd_pins FIFOMGR_0/LT] [get_bd_pins mstrigger_0/LAT] [get_bd_pins timestamp_1/latency]
  connect_bd_net -net mstrigger_0_TS [get_bd_pins FIFOMGR_0/TS] [get_bd_pins mstrigger_0/TS] [get_bd_pins timestamp_1/trig]
  connect_bd_net -net sclk_1 [get_bd_pins sclk] [get_bd_pins timer_0/sclk] [get_bd_pins timestamp_1/sclk]
  connect_bd_net -net threshold_1 [get_bd_pins threshold] [get_bd_pins ctrl_reg2/Din]
  connect_bd_net -net timer_0_ft [get_bd_pins FIFOMGR_0/TSF] [get_bd_pins timer_0/ft]
  connect_bd_net -net timer_0_st [get_bd_pins FIFOMGR_0/TSS] [get_bd_pins timer_0/st]
  connect_bd_net -net timestamp_1_trig_signal [get_bd_pins trigger] [get_bd_pins timestamp_1/trig_signal]
  connect_bd_net -net timestamp_1_tsf [get_bd_pins tsf] [get_bd_pins timestamp_1/tsf]
  connect_bd_net -net timestamp_1_tss [get_bd_pins tss] [get_bd_pins timestamp_1/tss]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconcat_1_dout [get_bd_pins xlconcat_1/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins ctrl_reg/Dout] [get_bd_pins mstrigger_0/en]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ADC_Controller
proc create_hier_cell_ADC_Controller { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ADC_Controller() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 31 -to 0 Ctrl_reg_in
  create_bd_pin -dir O -from 31 -to 0 Ctrl_reg_out
  create_bd_pin -dir O -from 15 -to 0 adc_raw
  create_bd_pin -dir I -from 15 -to 0 data_from_adc_DS_N
  create_bd_pin -dir I -from 15 -to 0 data_from_adc_DS_P
  create_bd_pin -dir I -from 0 -to 0 from_adc_or_DS_N
  create_bd_pin -dir I -from 0 -to 0 from_adc_or_DS_P
  create_bd_pin -dir I x_from_adc_calrun_fmc
  create_bd_pin -dir O x_to_adc_cal_fmc
  create_bd_pin -dir O x_to_adc_caldly_nscs_fmc
  create_bd_pin -dir O x_to_adc_dclk_rst_fmc
  create_bd_pin -dir O x_to_adc_fsr_ece_fmc
  create_bd_pin -dir O x_to_adc_led_0
  create_bd_pin -dir O x_to_adc_led_1
  create_bd_pin -dir O x_to_adc_outedge_ddr_sdata_fmc
  create_bd_pin -dir O x_to_adc_outv_slck_fmc
  create_bd_pin -dir O x_to_adc_pd_fmc

  # Create instance: adc_controller_0, and set properties
  set block_name adc_controller
  set block_cell_name adc_controller_0
  if { [catch {set adc_controller_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $adc_controller_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: util_ds_buf_2, and set properties
  set util_ds_buf_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_2 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {IBUFDS} \
   CONFIG.C_SIZE {16} \
 ] $util_ds_buf_2

  # Create instance: util_ds_buf_3, and set properties
  set util_ds_buf_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_3 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {IBUFDS} \
 ] $util_ds_buf_3

  # Create port connections
  connect_bd_net -net Ctrl_reg_in_1 [get_bd_pins Ctrl_reg_in] [get_bd_pins adc_controller_0/Ctrl_reg_in]
  connect_bd_net -net IBUF_DS_N1_1 [get_bd_pins from_adc_or_DS_N] [get_bd_pins util_ds_buf_3/IBUF_DS_N]
  connect_bd_net -net IBUF_DS_N_2 [get_bd_pins data_from_adc_DS_N] [get_bd_pins util_ds_buf_2/IBUF_DS_N]
  connect_bd_net -net IBUF_DS_P1_1 [get_bd_pins from_adc_or_DS_P] [get_bd_pins util_ds_buf_3/IBUF_DS_P]
  connect_bd_net -net IBUF_DS_P_2 [get_bd_pins data_from_adc_DS_P] [get_bd_pins util_ds_buf_2/IBUF_DS_P]
  connect_bd_net -net adc_controller_0_Ctrl_reg_out [get_bd_pins Ctrl_reg_out] [get_bd_pins adc_controller_0/Ctrl_reg_out]
  connect_bd_net -net adc_controller_0_x_to_adc_cal_fmc [get_bd_pins x_to_adc_cal_fmc] [get_bd_pins adc_controller_0/x_to_adc_cal_fmc]
  connect_bd_net -net adc_controller_0_x_to_adc_caldly_nscs_fmc [get_bd_pins x_to_adc_caldly_nscs_fmc] [get_bd_pins adc_controller_0/x_to_adc_caldly_nscs_fmc]
  connect_bd_net -net adc_controller_0_x_to_adc_dclk_rst_fmc [get_bd_pins x_to_adc_dclk_rst_fmc] [get_bd_pins adc_controller_0/x_to_adc_dclk_rst_fmc]
  connect_bd_net -net adc_controller_0_x_to_adc_fsr_ece_fmc [get_bd_pins x_to_adc_fsr_ece_fmc] [get_bd_pins adc_controller_0/x_to_adc_fsr_ece_fmc]
  connect_bd_net -net adc_controller_0_x_to_adc_led_0 [get_bd_pins x_to_adc_led_0] [get_bd_pins adc_controller_0/x_to_adc_led_0]
  connect_bd_net -net adc_controller_0_x_to_adc_led_1 [get_bd_pins x_to_adc_led_1] [get_bd_pins adc_controller_0/x_to_adc_led_1]
  connect_bd_net -net adc_controller_0_x_to_adc_outedge_ddr_sdata_fmc [get_bd_pins x_to_adc_outedge_ddr_sdata_fmc] [get_bd_pins adc_controller_0/x_to_adc_outedge_ddr_sdata_fmc]
  connect_bd_net -net adc_controller_0_x_to_adc_outv_slck_fmc [get_bd_pins x_to_adc_outv_slck_fmc] [get_bd_pins adc_controller_0/x_to_adc_outv_slck_fmc]
  connect_bd_net -net adc_controller_0_x_to_adc_pd_fmc [get_bd_pins x_to_adc_pd_fmc] [get_bd_pins adc_controller_0/x_to_adc_pd_fmc]
  connect_bd_net -net util_ds_buf_2_IBUF_OUT [get_bd_pins adc_raw] [get_bd_pins util_ds_buf_2/IBUF_OUT]
  connect_bd_net -net util_ds_buf_3_IBUF_OUT [get_bd_pins adc_controller_0/x_from_adc_or] [get_bd_pins util_ds_buf_3/IBUF_OUT]
  connect_bd_net -net x_from_adc_calrun_fmc_1 [get_bd_pins x_from_adc_calrun_fmc] [get_bd_pins adc_controller_0/x_from_adc_calrun_fmc]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: DMA_Osciloscope
proc create_hier_cell_DMA_Osciloscope { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_DMA_Osciloscope() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -type clk GCLK
  create_bd_pin -dir I -type rst GRST
  create_bd_pin -dir I data_valid
  create_bd_pin -dir I -from 15 -to 0 din

  # Create instance: AXI_Trigger_gen_m4_0, and set properties
  set AXI_Trigger_gen_m4_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:AXI_Trigger_gen_m4:1.0 AXI_Trigger_gen_m4_0 ]

  # Create instance: FIFO_SIZE, and set properties
  set FIFO_SIZE [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 FIFO_SIZE ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {32768} \
   CONFIG.CONST_WIDTH {32} \
 ] $FIFO_SIZE

  # Create instance: OLD_fifo, and set properties
  set OLD_fifo [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 OLD_fifo ]
  set_property -dict [ list \
   CONFIG.Clock_Type_AXI {Independent_Clock} \
   CONFIG.Empty_Threshold_Assert_Value_axis {4} \
   CONFIG.Empty_Threshold_Assert_Value_rach {13} \
   CONFIG.Empty_Threshold_Assert_Value_rdch {1021} \
   CONFIG.Empty_Threshold_Assert_Value_wach {13} \
   CONFIG.Empty_Threshold_Assert_Value_wdch {1021} \
   CONFIG.Empty_Threshold_Assert_Value_wrch {13} \
   CONFIG.Enable_Data_Counts_axis {false} \
   CONFIG.Enable_Safety_Circuit {true} \
   CONFIG.FIFO_Implementation_axis {Independent_Clocks_Block_RAM} \
   CONFIG.FIFO_Implementation_rach {Independent_Clocks_Distributed_RAM} \
   CONFIG.FIFO_Implementation_rdch {Independent_Clocks_Block_RAM} \
   CONFIG.FIFO_Implementation_wach {Independent_Clocks_Distributed_RAM} \
   CONFIG.FIFO_Implementation_wdch {Independent_Clocks_Block_RAM} \
   CONFIG.FIFO_Implementation_wrch {Independent_Clocks_Distributed_RAM} \
   CONFIG.Fifo_Implementation {Common_Clock_Block_RAM} \
   CONFIG.Full_Flags_Reset_Value {1} \
   CONFIG.Full_Threshold_Assert_Value_axis {32767} \
   CONFIG.Full_Threshold_Assert_Value_rach {15} \
   CONFIG.Full_Threshold_Assert_Value_wach {15} \
   CONFIG.Full_Threshold_Assert_Value_wrch {15} \
   CONFIG.INTERFACE_TYPE {AXI_STREAM} \
   CONFIG.Input_Depth_axis {32768} \
   CONFIG.Programmable_Empty_Type_axis {Single_Programmable_Empty_Threshold_Constant} \
   CONFIG.Programmable_Full_Type_axis {Single_Programmable_Full_Threshold_Constant} \
   CONFIG.Reset_Type {Asynchronous_Reset} \
   CONFIG.TDATA_NUM_BYTES {4} \
   CONFIG.TKEEP_WIDTH {4} \
   CONFIG.TSTRB_WIDTH {4} \
   CONFIG.TUSER_WIDTH {0} \
   CONFIG.Use_Embedded_Registers {false} \
 ] $OLD_fifo

  # Create instance: axi_dma_0, and set properties
  set axi_dma_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0 ]
  set_property -dict [ list \
   CONFIG.c_include_mm2s {0} \
   CONFIG.c_include_s2mm {1} \
   CONFIG.c_include_s2mm_dre {1} \
   CONFIG.c_include_sg {0} \
   CONFIG.c_m_axi_s2mm_data_width {64} \
   CONFIG.c_s2mm_burst_size {256} \
   CONFIG.c_sg_include_stscntrl_strm {0} \
   CONFIG.c_sg_length_width {23} \
 ] $axi_dma_0

  # Create instance: empty, and set properties
  set empty [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 empty ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $empty

  # Create instance: full, and set properties
  set full [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 full ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $full

  # Create instance: tlast_gen_0, and set properties
  set tlast_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:tlast_gen:1.0 tlast_gen_0 ]
  set_property -dict [ list \
   CONFIG.MAX_PKT_LENGTH {2147483647} \
   CONFIG.TDATA_WIDTH {32} \
 ] $tlast_gen_0

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.IN0_WIDTH {16} \
   CONFIG.IN1_WIDTH {16} \
 ] $xlconcat_0

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {15} \
   CONFIG.CONST_WIDTH {4} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {16} \
 ] $xlconstant_1

  # Create interface connections
  connect_bd_intf_net -intf_net tlast_gen_0_m_axis [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM] [get_bd_intf_pins tlast_gen_0/m_axis]

  # Create port connections
  connect_bd_net -net AXI_Trigger_gen_m4_0_fifo_m_axis_tready [get_bd_pins AXI_Trigger_gen_m4_0/fifo_m_axis_tready] [get_bd_pins OLD_fifo/m_axis_tready]
  connect_bd_net -net AXI_Trigger_gen_m4_0_fifo_we [get_bd_pins AXI_Trigger_gen_m4_0/fifo_we] [get_bd_pins OLD_fifo/s_axis_tvalid]
  connect_bd_net -net Net [get_bd_pins GCLK] [get_bd_pins AXI_Trigger_gen_m4_0/Trig_clk] [get_bd_pins OLD_fifo/m_aclk] [get_bd_pins OLD_fifo/s_aclk] [get_bd_pins axi_dma_0/m_axi_s2mm_aclk] [get_bd_pins tlast_gen_0/aclk]
  connect_bd_net -net OLD_fifo_m_axis_tdata [get_bd_pins OLD_fifo/m_axis_tdata] [get_bd_pins tlast_gen_0/s_axis_tdata]
  connect_bd_net -net OLD_fifo_m_axis_tvalid [get_bd_pins OLD_fifo/m_axis_tvalid] [get_bd_pins empty/Op1] [get_bd_pins tlast_gen_0/s_axis_tvalid]
  connect_bd_net -net OLD_fifo_s_axis_tready [get_bd_pins OLD_fifo/s_axis_tready] [get_bd_pins full/Op1]
  connect_bd_net -net data_valid_1 [get_bd_pins data_valid] [get_bd_pins AXI_Trigger_gen_m4_0/data_valid]
  connect_bd_net -net din_1 [get_bd_pins din] [get_bd_pins AXI_Trigger_gen_m4_0/din] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net empty_Res [get_bd_pins AXI_Trigger_gen_m4_0/fifo_empty] [get_bd_pins empty/Res]
  connect_bd_net -net full_Res [get_bd_pins AXI_Trigger_gen_m4_0/fifo_full] [get_bd_pins full/Res]
  connect_bd_net -net resetn_1 [get_bd_pins GRST] [get_bd_pins AXI_Trigger_gen_m4_0/Trig_resetn] [get_bd_pins OLD_fifo/s_aresetn] [get_bd_pins tlast_gen_0/resetn]
  connect_bd_net -net tlast_gen_0_s_axis_tready [get_bd_pins AXI_Trigger_gen_m4_0/dma_tready] [get_bd_pins tlast_gen_0/s_axis_tready]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins OLD_fifo/s_axis_tdata] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins axi_dma_0/s_axis_s2mm_tkeep] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins FIFO_SIZE/dout] [get_bd_pins tlast_gen_0/pkt_length]
  connect_bd_net -net xlconstant_1_dout_1 [get_bd_pins xlconcat_0/In1] [get_bd_pins xlconstant_1/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: GP_IC
proc create_hier_cell_GP_IC { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_GP_IC() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I CLK
  create_bd_pin -dir I -from 31 -to 0 CTRL
  create_bd_pin -dir I -from 31 -to 0 Ctrl_reg_in
  create_bd_pin -dir O -from 31 -to 0 Ctrl_reg_out
  create_bd_pin -dir I -from 31 -to 0 DATA
  create_bd_pin -dir I -from 31 -to 0 DEVICE
  create_bd_pin -dir O -from 15 -to 0 D_sum
  create_bd_pin -dir O Decim_data_valid
  create_bd_pin -dir I -from 31 -to 0 FIFOMGR_CTRL_REG
  create_bd_pin -dir I FIFO_AEMPTY
  create_bd_pin -dir I FIFO_AFULL
  create_bd_pin -dir O FIFO_CLR
  create_bd_pin -dir O -from 31 -to 0 FIFO_DATA
  create_bd_pin -dir I FIFO_EMPTY
  create_bd_pin -dir I FIFO_FULL
  create_bd_pin -dir O FIFO_WE
  create_bd_pin -dir I -from 31 -to 0 In0
  create_bd_pin -dir I -from 31 -to 0 In1
  create_bd_pin -dir I -from 31 -to 0 In2
  create_bd_pin -dir I -from 31 -to 0 In3
  create_bd_pin -dir I -from 15 -to 0 N
  create_bd_pin -dir O RLED_0
  create_bd_pin -dir I -type rst RST
  create_bd_pin -dir O SCK_0
  create_bd_pin -dir O SDATA_0
  create_bd_pin -dir O -from 0 -to 0 SS_0
  create_bd_pin -dir O -from 15 -to 0 addb_0
  create_bd_pin -dir I -from 31 -to 0 ctrl_reg
  create_bd_pin -dir I -from 31 -to 0 ctrl_reg_0
  create_bd_pin -dir O -from 31 -to 0 curr_cycle_0
  create_bd_pin -dir I -from 15 -to 0 data_from_adc_DS_N
  create_bd_pin -dir I -from 15 -to 0 data_from_adc_DS_P
  create_bd_pin -dir O -from 31 -to 0 dinb_0
  create_bd_pin -dir O done_0
  create_bd_pin -dir I -from 31 -to 0 doutb_0
  create_bd_pin -dir I -from 0 -to 0 from_adc_or_DS_N
  create_bd_pin -dir I -from 0 -to 0 from_adc_or_DS_P
  create_bd_pin -dir I -from 31 -to 0 max_cycles_0
  create_bd_pin -dir I rtc_1hz
  create_bd_pin -dir I -from 31 -to 0 threshold
  create_bd_pin -dir O -from 31 -to 0 trigger
  create_bd_pin -dir O -from 31 -to 0 tsf
  create_bd_pin -dir O -from 31 -to 0 tss
  create_bd_pin -dir O -from 0 -to 0 web_0
  create_bd_pin -dir I x_from_adc_calrun_fmc
  create_bd_pin -dir O x_to_adc_cal_fmc
  create_bd_pin -dir O x_to_adc_caldly_nscs_fmc
  create_bd_pin -dir O x_to_adc_dclk_rst_fmc
  create_bd_pin -dir O x_to_adc_fsr_ece_fmc
  create_bd_pin -dir O x_to_adc_led_0
  create_bd_pin -dir O x_to_adc_led_1
  create_bd_pin -dir O x_to_adc_outedge_ddr_sdata_fmc
  create_bd_pin -dir O x_to_adc_outv_slck_fmc
  create_bd_pin -dir O x_to_adc_pd_fmc

  # Create instance: ADC_Controller
  create_hier_cell_ADC_Controller $hier_obj ADC_Controller

  # Create instance: ADC_Decimator_0, and set properties
  set ADC_Decimator_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:ADC_Decimator:1.0 ADC_Decimator_0 ]

  # Create instance: DAC_CTRL1v1_0, and set properties
  set block_name DAC_CTRL1v1
  set block_cell_name DAC_CTRL1v1_0
  if { [catch {set DAC_CTRL1v1_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $DAC_CTRL1v1_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.NSLAVES {1} \
 ] $DAC_CTRL1v1_0

  # Create instance: Hist_Gen_Block_0, and set properties
  set Hist_Gen_Block_0 [ create_bd_cell -type ip -vlnv ictp.it:user:Hist_Gen_Block:1.0 Hist_Gen_Block_0 ]

  # Create instance: timestamp
  create_hier_cell_timestamp $hier_obj timestamp

  # Create port connections
  connect_bd_net -net ADC_Controller_Ctrl_reg_out [get_bd_pins Ctrl_reg_out] [get_bd_pins ADC_Controller/Ctrl_reg_out]
  connect_bd_net -net ADC_Controller_adc_raw [get_bd_pins ADC_Controller/adc_raw] [get_bd_pins ADC_Decimator_0/Din] [get_bd_pins timestamp/din]
  connect_bd_net -net ADC_Controller_x_to_adc_cal_fmc [get_bd_pins x_to_adc_cal_fmc] [get_bd_pins ADC_Controller/x_to_adc_cal_fmc]
  connect_bd_net -net ADC_Controller_x_to_adc_caldly_nscs_fmc [get_bd_pins x_to_adc_caldly_nscs_fmc] [get_bd_pins ADC_Controller/x_to_adc_caldly_nscs_fmc]
  connect_bd_net -net ADC_Controller_x_to_adc_dclk_rst_fmc [get_bd_pins x_to_adc_dclk_rst_fmc] [get_bd_pins ADC_Controller/x_to_adc_dclk_rst_fmc]
  connect_bd_net -net ADC_Controller_x_to_adc_fsr_ece_fmc [get_bd_pins x_to_adc_fsr_ece_fmc] [get_bd_pins ADC_Controller/x_to_adc_fsr_ece_fmc]
  connect_bd_net -net ADC_Controller_x_to_adc_led_0 [get_bd_pins x_to_adc_led_0] [get_bd_pins ADC_Controller/x_to_adc_led_0]
  connect_bd_net -net ADC_Controller_x_to_adc_led_1 [get_bd_pins x_to_adc_led_1] [get_bd_pins ADC_Controller/x_to_adc_led_1]
  connect_bd_net -net ADC_Controller_x_to_adc_outedge_ddr_sdata_fmc [get_bd_pins x_to_adc_outedge_ddr_sdata_fmc] [get_bd_pins ADC_Controller/x_to_adc_outedge_ddr_sdata_fmc]
  connect_bd_net -net ADC_Controller_x_to_adc_outv_slck_fmc [get_bd_pins x_to_adc_outv_slck_fmc] [get_bd_pins ADC_Controller/x_to_adc_outv_slck_fmc]
  connect_bd_net -net ADC_Controller_x_to_adc_pd_fmc [get_bd_pins x_to_adc_pd_fmc] [get_bd_pins ADC_Controller/x_to_adc_pd_fmc]
  connect_bd_net -net CTRL_1 [get_bd_pins CTRL] [get_bd_pins DAC_CTRL1v1_0/CTRL]
  connect_bd_net -net Comblock_PL_D_sum [get_bd_pins D_sum] [get_bd_pins ADC_Decimator_0/D_sum] [get_bd_pins Hist_Gen_Block_0/in_addr_0]
  connect_bd_net -net Comblock_PL_Decim_data_valid [get_bd_pins Decim_data_valid] [get_bd_pins ADC_Decimator_0/Decim_data_valid] [get_bd_pins Hist_Gen_Block_0/data_valid_0]
  connect_bd_net -net Comblock_PL_RLED_0 [get_bd_pins RLED_0] [get_bd_pins DAC_CTRL1v1_0/RLED]
  connect_bd_net -net Comblock_PL_SCK_0 [get_bd_pins SCK_0] [get_bd_pins DAC_CTRL1v1_0/SCK]
  connect_bd_net -net Comblock_PL_SDATA_0 [get_bd_pins SDATA_0] [get_bd_pins DAC_CTRL1v1_0/SDATA]
  connect_bd_net -net Comblock_PL_SS_0 [get_bd_pins SS_0] [get_bd_pins DAC_CTRL1v1_0/SS]
  connect_bd_net -net Ctrl_reg_in_1 [get_bd_pins Ctrl_reg_in] [get_bd_pins ADC_Controller/Ctrl_reg_in]
  connect_bd_net -net DATA_1 [get_bd_pins DATA] [get_bd_pins DAC_CTRL1v1_0/DATA]
  connect_bd_net -net DEVICE_1 [get_bd_pins DEVICE] [get_bd_pins DAC_CTRL1v1_0/DEVICE]
  connect_bd_net -net FIFOMGR_CTRL_REG_1 [get_bd_pins FIFOMGR_CTRL_REG] [get_bd_pins timestamp/FIFOMGR_CTRL_REG]
  connect_bd_net -net FIFO_AEMPTY_1 [get_bd_pins FIFO_AEMPTY] [get_bd_pins timestamp/FIFO_AEMPTY]
  connect_bd_net -net FIFO_AFULL_1 [get_bd_pins FIFO_AFULL] [get_bd_pins timestamp/FIFO_AFULL]
  connect_bd_net -net FIFO_EMPTY_1 [get_bd_pins FIFO_EMPTY] [get_bd_pins timestamp/FIFO_EMPTY]
  connect_bd_net -net FIFO_FULL_1 [get_bd_pins FIFO_FULL] [get_bd_pins timestamp/FIFO_FULL]
  connect_bd_net -net Hist_Gen_Block_0_addb_0 [get_bd_pins addb_0] [get_bd_pins Hist_Gen_Block_0/addb_0]
  connect_bd_net -net Hist_Gen_Block_0_curr_cycle_0 [get_bd_pins curr_cycle_0] [get_bd_pins Hist_Gen_Block_0/curr_cycle_0]
  connect_bd_net -net Hist_Gen_Block_0_dinb_0 [get_bd_pins dinb_0] [get_bd_pins Hist_Gen_Block_0/dinb_0]
  connect_bd_net -net Hist_Gen_Block_0_done_0 [get_bd_pins done_0] [get_bd_pins Hist_Gen_Block_0/done_0]
  connect_bd_net -net Hist_Gen_Block_0_web_0 [get_bd_pins web_0] [get_bd_pins Hist_Gen_Block_0/web_0]
  connect_bd_net -net In0_1 [get_bd_pins In0] [get_bd_pins timestamp/In0]
  connect_bd_net -net In1_1 [get_bd_pins In1] [get_bd_pins timestamp/In1]
  connect_bd_net -net In2_1 [get_bd_pins In2] [get_bd_pins timestamp/In2]
  connect_bd_net -net In3_1 [get_bd_pins In3] [get_bd_pins timestamp/In3]
  connect_bd_net -net N_1 [get_bd_pins N] [get_bd_pins ADC_Decimator_0/N]
  connect_bd_net -net RST_1 [get_bd_pins RST] [get_bd_pins ADC_Decimator_0/resetn] [get_bd_pins DAC_CTRL1v1_0/RST] [get_bd_pins Hist_Gen_Block_0/rst_0] [get_bd_pins timestamp/rst]
  connect_bd_net -net ctrl_reg_0_1 [get_bd_pins ctrl_reg_0] [get_bd_pins Hist_Gen_Block_0/ctrl_reg_0]
  connect_bd_net -net ctrl_reg_1 [get_bd_pins ctrl_reg] [get_bd_pins timestamp/ctrl_reg]
  connect_bd_net -net data_from_adc_DS_N_1 [get_bd_pins data_from_adc_DS_N] [get_bd_pins ADC_Controller/data_from_adc_DS_N]
  connect_bd_net -net data_from_adc_DS_P_1 [get_bd_pins data_from_adc_DS_P] [get_bd_pins ADC_Controller/data_from_adc_DS_P]
  connect_bd_net -net doutb_0_1 [get_bd_pins doutb_0] [get_bd_pins Hist_Gen_Block_0/doutb_0]
  connect_bd_net -net fifo_clk_i_1 [get_bd_pins CLK] [get_bd_pins ADC_Decimator_0/clk_in] [get_bd_pins DAC_CTRL1v1_0/CLK] [get_bd_pins Hist_Gen_Block_0/clk_0] [get_bd_pins timestamp/fclk]
  connect_bd_net -net from_adc_or_DS_N_1 [get_bd_pins from_adc_or_DS_N] [get_bd_pins ADC_Controller/from_adc_or_DS_N]
  connect_bd_net -net from_adc_or_DS_P_1 [get_bd_pins from_adc_or_DS_P] [get_bd_pins ADC_Controller/from_adc_or_DS_P]
  connect_bd_net -net max_cycles_0_1 [get_bd_pins max_cycles_0] [get_bd_pins Hist_Gen_Block_0/max_cycles_0]
  connect_bd_net -net rtc_1hz_1 [get_bd_pins rtc_1hz] [get_bd_pins timestamp/sclk]
  connect_bd_net -net threshold_1 [get_bd_pins threshold] [get_bd_pins timestamp/threshold]
  connect_bd_net -net timestamp_FIFO_CLR [get_bd_pins FIFO_CLR] [get_bd_pins timestamp/FIFO_CLR]
  connect_bd_net -net timestamp_FIFO_DATA [get_bd_pins FIFO_DATA] [get_bd_pins timestamp/FIFO_DATA]
  connect_bd_net -net timestamp_FIFO_WE [get_bd_pins FIFO_WE] [get_bd_pins timestamp/FIFO_WE]
  connect_bd_net -net timestamp_trigger [get_bd_pins trigger] [get_bd_pins timestamp/trigger]
  connect_bd_net -net timestamp_tsf [get_bd_pins tsf] [get_bd_pins timestamp/tsf]
  connect_bd_net -net timestamp_tss [get_bd_pins tss] [get_bd_pins timestamp/tss]
  connect_bd_net -net x_from_adc_calrun_fmc_1 [get_bd_pins x_from_adc_calrun_fmc] [get_bd_pins ADC_Controller/x_from_adc_calrun_fmc]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: AXI_IC
proc create_hier_cell_AXI_IC { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_AXI_IC() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -type clk GCLK
  create_bd_pin -dir I -type rst GRST
  create_bd_pin -dir I data_valid
  create_bd_pin -dir I -from 15 -to 0 din
  create_bd_pin -dir O fifo_aempty_o
  create_bd_pin -dir O fifo_afull_o
  create_bd_pin -dir I fifo_clear_i
  create_bd_pin -dir I -from 31 -to 0 fifo_data_i
  create_bd_pin -dir O fifo_empty_o
  create_bd_pin -dir O fifo_full_o
  create_bd_pin -dir I fifo_we_i
  create_bd_pin -dir I -from 15 -to 0 ram_addr_i
  create_bd_pin -dir I -from 31 -to 0 ram_data_i
  create_bd_pin -dir O -from 31 -to 0 ram_data_o
  create_bd_pin -dir I ram_we_i
  create_bd_pin -dir I -from 31 -to 0 reg0_i
  create_bd_pin -dir O -from 31 -to 0 reg0_o
  create_bd_pin -dir O -from 31 -to 0 reg10_o
  create_bd_pin -dir O -from 31 -to 0 reg11_o
  create_bd_pin -dir O -from 31 -to 0 reg12_o
  create_bd_pin -dir O -from 31 -to 0 reg13_o
  create_bd_pin -dir O -from 31 -to 0 reg14_o
  create_bd_pin -dir I -from 31 -to 0 reg1_i
  create_bd_pin -dir O -from 31 -to 0 reg1_o
  create_bd_pin -dir I -from 31 -to 0 reg2_i
  create_bd_pin -dir I -from 31 -to 0 reg3_i
  create_bd_pin -dir O -from 31 -to 0 reg3_o
  create_bd_pin -dir I -from 31 -to 0 reg4_i
  create_bd_pin -dir O -from 31 -to 0 reg4_o
  create_bd_pin -dir I -from 31 -to 0 reg5_i
  create_bd_pin -dir O -from 31 -to 0 reg5_o
  create_bd_pin -dir O -from 31 -to 0 reg6_o
  create_bd_pin -dir O -from 31 -to 0 reg7_o
  create_bd_pin -dir O -from 31 -to 0 reg8_o
  create_bd_pin -dir O -from 31 -to 0 reg9_o

  # Create instance: DMA_Osciloscope
  create_hier_cell_DMA_Osciloscope $hier_obj DMA_Osciloscope

  # Create instance: comblock_0, and set properties
  set comblock_0 [ create_bd_cell -type ip -vlnv user.org:user:comblock:1.0 comblock_0 ]
  set_property -dict [ list \
   CONFIG.C_FIFO_DATA_WIDTH {32} \
   CONFIG.C_FIFO_DEPTH {1533} \
 ] $comblock_0

  # Create port connections
  connect_bd_net -net CLK_1 [get_bd_pins GCLK] [get_bd_pins DMA_Osciloscope/GCLK] [get_bd_pins comblock_0/fifo_clk_i] [get_bd_pins comblock_0/ram_clk_i]
  connect_bd_net -net PL_Ctrl_reg_out [get_bd_pins reg3_i] [get_bd_pins comblock_0/reg3_i]
  connect_bd_net -net PL_FIFO_CLR [get_bd_pins fifo_clear_i] [get_bd_pins comblock_0/fifo_clear_i]
  connect_bd_net -net PL_FIFO_DATA [get_bd_pins fifo_data_i] [get_bd_pins comblock_0/fifo_data_i]
  connect_bd_net -net PL_FIFO_WE [get_bd_pins fifo_we_i] [get_bd_pins comblock_0/fifo_we_i]
  connect_bd_net -net PL_addb_0 [get_bd_pins ram_addr_i] [get_bd_pins comblock_0/ram_addr_i]
  connect_bd_net -net PL_curr_cycle_0 [get_bd_pins reg4_i] [get_bd_pins comblock_0/reg4_i]
  connect_bd_net -net PL_dinb_0 [get_bd_pins ram_data_i] [get_bd_pins comblock_0/ram_data_i]
  connect_bd_net -net PL_done_0 [get_bd_pins reg5_i] [get_bd_pins comblock_0/reg5_i]
  connect_bd_net -net PL_trigger [get_bd_pins reg0_i] [get_bd_pins comblock_0/reg0_i]
  connect_bd_net -net PL_tsf [get_bd_pins reg2_i] [get_bd_pins comblock_0/reg2_i]
  connect_bd_net -net PL_tss [get_bd_pins reg1_i] [get_bd_pins comblock_0/reg1_i]
  connect_bd_net -net PL_web_0 [get_bd_pins ram_we_i] [get_bd_pins comblock_0/ram_we_i]
  connect_bd_net -net comblock_0_fifo_aempty_o [get_bd_pins fifo_aempty_o] [get_bd_pins comblock_0/fifo_aempty_o]
  connect_bd_net -net comblock_0_fifo_afull_o [get_bd_pins fifo_afull_o] [get_bd_pins comblock_0/fifo_afull_o]
  connect_bd_net -net comblock_0_fifo_empty_o [get_bd_pins fifo_empty_o] [get_bd_pins comblock_0/fifo_empty_o]
  connect_bd_net -net comblock_0_fifo_full_o [get_bd_pins fifo_full_o] [get_bd_pins comblock_0/fifo_full_o]
  connect_bd_net -net comblock_0_ram_data_o [get_bd_pins ram_data_o] [get_bd_pins comblock_0/ram_data_o]
  connect_bd_net -net comblock_0_reg0_o [get_bd_pins reg0_o] [get_bd_pins comblock_0/reg0_o]
  connect_bd_net -net comblock_0_reg10_o [get_bd_pins reg10_o] [get_bd_pins comblock_0/reg10_o]
  connect_bd_net -net comblock_0_reg11_o [get_bd_pins reg11_o] [get_bd_pins comblock_0/reg11_o]
  connect_bd_net -net comblock_0_reg12_o [get_bd_pins reg12_o] [get_bd_pins comblock_0/reg12_o]
  connect_bd_net -net comblock_0_reg13_o [get_bd_pins reg13_o] [get_bd_pins comblock_0/reg13_o]
  connect_bd_net -net comblock_0_reg14_o [get_bd_pins reg14_o] [get_bd_pins comblock_0/reg14_o]
  connect_bd_net -net comblock_0_reg1_o [get_bd_pins reg1_o] [get_bd_pins comblock_0/reg1_o]
  connect_bd_net -net comblock_0_reg3_o [get_bd_pins reg3_o] [get_bd_pins comblock_0/reg3_o]
  connect_bd_net -net comblock_0_reg4_o [get_bd_pins reg4_o] [get_bd_pins comblock_0/reg4_o]
  connect_bd_net -net comblock_0_reg5_o [get_bd_pins reg5_o] [get_bd_pins comblock_0/reg5_o]
  connect_bd_net -net comblock_0_reg6_o [get_bd_pins reg6_o] [get_bd_pins comblock_0/reg6_o]
  connect_bd_net -net comblock_0_reg7_o [get_bd_pins reg7_o] [get_bd_pins comblock_0/reg7_o]
  connect_bd_net -net comblock_0_reg8_o [get_bd_pins reg8_o] [get_bd_pins comblock_0/reg8_o]
  connect_bd_net -net comblock_0_reg9_o [get_bd_pins reg9_o] [get_bd_pins comblock_0/reg9_o]
  connect_bd_net -net data_valid_1 [get_bd_pins data_valid] [get_bd_pins DMA_Osciloscope/data_valid]
  connect_bd_net -net din_1 [get_bd_pins din] [get_bd_pins DMA_Osciloscope/din]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins GRST] [get_bd_pins DMA_Osciloscope/GRST]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: PL
proc create_hier_cell_PL { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_PL() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -type clk CLK
  create_bd_pin -dir O RLED_0
  create_bd_pin -dir I -type rst RST
  create_bd_pin -dir O SCK_0
  create_bd_pin -dir O SDATA_0
  create_bd_pin -dir O -from 0 -to 0 SS_0
  create_bd_pin -dir I -from 15 -to 0 data_from_adc_DS_N
  create_bd_pin -dir I -from 15 -to 0 data_from_adc_DS_P
  create_bd_pin -dir I -from 0 -to 0 from_adc_or_DS_N
  create_bd_pin -dir I -from 0 -to 0 from_adc_or_DS_P
  create_bd_pin -dir I rtc_1hz
  create_bd_pin -dir I x_from_adc_calrun_fmc
  create_bd_pin -dir O x_to_adc_cal_fmc
  create_bd_pin -dir O x_to_adc_caldly_nscs_fmc
  create_bd_pin -dir O x_to_adc_dclk_rst_fmc
  create_bd_pin -dir O x_to_adc_fsr_ece_fmc
  create_bd_pin -dir O x_to_adc_led_0
  create_bd_pin -dir O x_to_adc_led_1
  create_bd_pin -dir O x_to_adc_outedge_ddr_sdata_fmc
  create_bd_pin -dir O x_to_adc_outv_slck_fmc
  create_bd_pin -dir O x_to_adc_pd_fmc

  # Create instance: AXI_IC
  create_hier_cell_AXI_IC $hier_obj AXI_IC

  # Create instance: GP_IC
  create_hier_cell_GP_IC $hier_obj GP_IC

  # Create port connections
  connect_bd_net -net CLK_1 [get_bd_pins CLK] [get_bd_pins AXI_IC/GCLK] [get_bd_pins GP_IC/CLK]
  connect_bd_net -net PL_Ctrl_reg_out [get_bd_pins AXI_IC/reg3_i] [get_bd_pins GP_IC/Ctrl_reg_out]
  connect_bd_net -net PL_FIFO_CLR [get_bd_pins AXI_IC/fifo_clear_i] [get_bd_pins GP_IC/FIFO_CLR]
  connect_bd_net -net PL_FIFO_DATA [get_bd_pins AXI_IC/fifo_data_i] [get_bd_pins GP_IC/FIFO_DATA]
  connect_bd_net -net PL_FIFO_WE [get_bd_pins AXI_IC/fifo_we_i] [get_bd_pins GP_IC/FIFO_WE]
  connect_bd_net -net PL_RLED_0 [get_bd_pins RLED_0] [get_bd_pins GP_IC/RLED_0]
  connect_bd_net -net PL_SCK_0 [get_bd_pins SCK_0] [get_bd_pins GP_IC/SCK_0]
  connect_bd_net -net PL_SDATA_0 [get_bd_pins SDATA_0] [get_bd_pins GP_IC/SDATA_0]
  connect_bd_net -net PL_SS_0 [get_bd_pins SS_0] [get_bd_pins GP_IC/SS_0]
  connect_bd_net -net PL_addb_0 [get_bd_pins AXI_IC/ram_addr_i] [get_bd_pins GP_IC/addb_0]
  connect_bd_net -net PL_curr_cycle_0 [get_bd_pins AXI_IC/reg4_i] [get_bd_pins GP_IC/curr_cycle_0]
  connect_bd_net -net PL_dinb_0 [get_bd_pins AXI_IC/ram_data_i] [get_bd_pins GP_IC/dinb_0]
  connect_bd_net -net PL_done_0 [get_bd_pins AXI_IC/reg5_i] [get_bd_pins GP_IC/done_0]
  connect_bd_net -net PL_trigger [get_bd_pins AXI_IC/reg0_i] [get_bd_pins GP_IC/trigger]
  connect_bd_net -net PL_tsf [get_bd_pins AXI_IC/reg2_i] [get_bd_pins GP_IC/tsf]
  connect_bd_net -net PL_tss [get_bd_pins AXI_IC/reg1_i] [get_bd_pins GP_IC/tss]
  connect_bd_net -net PL_web_0 [get_bd_pins AXI_IC/ram_we_i] [get_bd_pins GP_IC/web_0]
  connect_bd_net -net PL_x_to_adc_cal_fmc [get_bd_pins x_to_adc_cal_fmc] [get_bd_pins GP_IC/x_to_adc_cal_fmc]
  connect_bd_net -net PL_x_to_adc_caldly_nscs_fmc [get_bd_pins x_to_adc_caldly_nscs_fmc] [get_bd_pins GP_IC/x_to_adc_caldly_nscs_fmc]
  connect_bd_net -net PL_x_to_adc_dclk_rst_fmc [get_bd_pins x_to_adc_dclk_rst_fmc] [get_bd_pins GP_IC/x_to_adc_dclk_rst_fmc]
  connect_bd_net -net PL_x_to_adc_fsr_ece_fmc [get_bd_pins x_to_adc_fsr_ece_fmc] [get_bd_pins GP_IC/x_to_adc_fsr_ece_fmc]
  connect_bd_net -net PL_x_to_adc_led_0 [get_bd_pins x_to_adc_led_0] [get_bd_pins GP_IC/x_to_adc_led_0]
  connect_bd_net -net PL_x_to_adc_led_1 [get_bd_pins x_to_adc_led_1] [get_bd_pins GP_IC/x_to_adc_led_1]
  connect_bd_net -net PL_x_to_adc_outedge_ddr_sdata_fmc [get_bd_pins x_to_adc_outedge_ddr_sdata_fmc] [get_bd_pins GP_IC/x_to_adc_outedge_ddr_sdata_fmc]
  connect_bd_net -net PL_x_to_adc_outv_slck_fmc [get_bd_pins x_to_adc_outv_slck_fmc] [get_bd_pins GP_IC/x_to_adc_outv_slck_fmc]
  connect_bd_net -net PL_x_to_adc_pd_fmc [get_bd_pins x_to_adc_pd_fmc] [get_bd_pins GP_IC/x_to_adc_pd_fmc]
  connect_bd_net -net comblock_0_fifo_aempty_o [get_bd_pins AXI_IC/fifo_aempty_o] [get_bd_pins GP_IC/FIFO_AEMPTY]
  connect_bd_net -net comblock_0_fifo_afull_o [get_bd_pins AXI_IC/fifo_afull_o] [get_bd_pins GP_IC/FIFO_AFULL]
  connect_bd_net -net comblock_0_fifo_empty_o [get_bd_pins AXI_IC/fifo_empty_o] [get_bd_pins GP_IC/FIFO_EMPTY]
  connect_bd_net -net comblock_0_fifo_full_o [get_bd_pins AXI_IC/fifo_full_o] [get_bd_pins GP_IC/FIFO_FULL]
  connect_bd_net -net comblock_0_ram_data_o [get_bd_pins AXI_IC/ram_data_o] [get_bd_pins GP_IC/doutb_0]
  connect_bd_net -net comblock_0_reg0_o [get_bd_pins AXI_IC/reg0_o] [get_bd_pins GP_IC/threshold]
  connect_bd_net -net comblock_0_reg10_o [get_bd_pins AXI_IC/reg10_o] [get_bd_pins GP_IC/FIFOMGR_CTRL_REG]
  connect_bd_net -net comblock_0_reg11_o [get_bd_pins AXI_IC/reg11_o] [get_bd_pins GP_IC/In0]
  connect_bd_net -net comblock_0_reg12_o [get_bd_pins AXI_IC/reg12_o] [get_bd_pins GP_IC/In1]
  connect_bd_net -net comblock_0_reg13_o [get_bd_pins AXI_IC/reg13_o] [get_bd_pins GP_IC/In2]
  connect_bd_net -net comblock_0_reg14_o [get_bd_pins AXI_IC/reg14_o] [get_bd_pins GP_IC/In3]
  connect_bd_net -net comblock_0_reg1_o [get_bd_pins AXI_IC/reg1_o] [get_bd_pins GP_IC/ctrl_reg]
  connect_bd_net -net comblock_0_reg3_o [get_bd_pins AXI_IC/reg3_o] [get_bd_pins GP_IC/N]
  connect_bd_net -net comblock_0_reg4_o [get_bd_pins AXI_IC/reg4_o] [get_bd_pins GP_IC/DATA]
  connect_bd_net -net comblock_0_reg5_o [get_bd_pins AXI_IC/reg5_o] [get_bd_pins GP_IC/CTRL]
  connect_bd_net -net comblock_0_reg6_o [get_bd_pins AXI_IC/reg6_o] [get_bd_pins GP_IC/DEVICE]
  connect_bd_net -net comblock_0_reg7_o [get_bd_pins AXI_IC/reg7_o] [get_bd_pins GP_IC/Ctrl_reg_in]
  connect_bd_net -net comblock_0_reg8_o [get_bd_pins AXI_IC/reg8_o] [get_bd_pins GP_IC/ctrl_reg_0]
  connect_bd_net -net comblock_0_reg9_o [get_bd_pins AXI_IC/reg9_o] [get_bd_pins GP_IC/max_cycles_0]
  connect_bd_net -net data_from_adc_DS_N_1 [get_bd_pins data_from_adc_DS_N] [get_bd_pins GP_IC/data_from_adc_DS_N]
  connect_bd_net -net data_from_adc_DS_P_1 [get_bd_pins data_from_adc_DS_P] [get_bd_pins GP_IC/data_from_adc_DS_P]
  connect_bd_net -net data_valid_1 [get_bd_pins AXI_IC/data_valid] [get_bd_pins GP_IC/Decim_data_valid]
  connect_bd_net -net din_1 [get_bd_pins AXI_IC/din] [get_bd_pins GP_IC/D_sum]
  connect_bd_net -net from_adc_or_DS_N_1 [get_bd_pins from_adc_or_DS_N] [get_bd_pins GP_IC/from_adc_or_DS_N]
  connect_bd_net -net from_adc_or_DS_P_1 [get_bd_pins from_adc_or_DS_P] [get_bd_pins GP_IC/from_adc_or_DS_P]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins RST] [get_bd_pins AXI_IC/GRST] [get_bd_pins GP_IC/RST]
  connect_bd_net -net rtc_1hz_1 [get_bd_pins rtc_1hz] [get_bd_pins GP_IC/rtc_1hz]
  connect_bd_net -net x_from_adc_calrun_fmc_1 [get_bd_pins x_from_adc_calrun_fmc] [get_bd_pins GP_IC/x_from_adc_calrun_fmc]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: GFCR
proc create_hier_cell_GFCR { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_GFCR() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 -type clk CLK_IN1_D_clk_n
  create_bd_pin -dir I -from 0 -to 0 -type clk CLK_IN1_D_clk_p
  create_bd_pin -dir O -from 0 -to 0 -type clk PL_CLK
  create_bd_pin -dir O -from 0 -to 0 -type rst PL_rst
  create_bd_pin -dir I -type rst ps_rst

  # Create instance: proc_sys_reset_1, and set properties
  set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_1 ]

  # Create instance: util_ds_buf_0, and set properties
  set util_ds_buf_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_0 ]

  # Create instance: util_ds_buf_1, and set properties
  set util_ds_buf_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_1 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {BUFG} \
 ] $util_ds_buf_1

  # Create port connections
  connect_bd_net -net CLK_IN1_D_clk_n_1 [get_bd_pins CLK_IN1_D_clk_n] [get_bd_pins util_ds_buf_0/IBUF_DS_N]
  connect_bd_net -net CLK_IN1_D_clk_p_1 [get_bd_pins CLK_IN1_D_clk_p] [get_bd_pins util_ds_buf_0/IBUF_DS_P]
  connect_bd_net -net ext_reset_in_1 [get_bd_pins ps_rst] [get_bd_pins proc_sys_reset_1/ext_reset_in]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins PL_rst] [get_bd_pins proc_sys_reset_1/peripheral_aresetn]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins util_ds_buf_0/IBUF_OUT] [get_bd_pins util_ds_buf_1/BUFG_I]
  connect_bd_net -net util_ds_buf_1_BUFG_O [get_bd_pins PL_CLK] [get_bd_pins proc_sys_reset_1/slowest_sync_clk] [get_bd_pins util_ds_buf_1/BUFG_O]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: ADC_CLK_GENERATOR
proc create_hier_cell_ADC_CLK_GENERATOR { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ADC_CLK_GENERATOR() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 user_si570_sysclk

  # Create pins
  create_bd_pin -dir I -type rst PERIPHERAL_SYS_RST
  create_bd_pin -dir O -from 0 -to 0 -type clk clk_to_adc_DS_N
  create_bd_pin -dir O -from 0 -to 0 -type clk clk_to_adc_DS_P

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {33.330000000000005} \
   CONFIG.CLKOUT1_JITTER {81.814} \
   CONFIG.CLKOUT1_PHASE_ERROR {77.836} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {300.000} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT5_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT6_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLKOUT7_REQUESTED_OUT_FREQ {100.000} \
   CONFIG.CLK_IN1_BOARD_INTERFACE {user_si570_sysclk} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {4.000} \
   CONFIG.MMCM_CLKIN1_PERIOD {3.333} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {4.000} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} \
   CONFIG.USE_BOARD_FLOW {true} \
   CONFIG.USE_FREQ_SYNTH {true} \
 ] $clk_wiz_0

  # Create instance: util_ds_buf_0, and set properties
  set util_ds_buf_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.1 util_ds_buf_0 ]
  set_property -dict [ list \
   CONFIG.C_BUF_TYPE {OBUFDS} \
 ] $util_ds_buf_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins user_si570_sysclk] [get_bd_intf_pins clk_wiz_0/CLK_IN1_D]

  # Create port connections
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins util_ds_buf_0/OBUF_IN]
  connect_bd_net -net s00_axi_aresetn1_1 [get_bd_pins PERIPHERAL_SYS_RST] [get_bd_pins clk_wiz_0/reset]
  connect_bd_net -net util_ds_buf_0_OBUF_DS_N [get_bd_pins clk_to_adc_DS_N] [get_bd_pins util_ds_buf_0/OBUF_DS_N]
  connect_bd_net -net util_ds_buf_0_OBUF_DS_P [get_bd_pins clk_to_adc_DS_P] [get_bd_pins util_ds_buf_0/OBUF_DS_P]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set IIC0 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 IIC0 ]
  set user_si570_sysclk [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 user_si570_sysclk ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {300000000} \
   ] $user_si570_sysclk

  # Create ports
  set CLK_IN1_D_clk_n [ create_bd_port -dir I -type clk CLK_IN1_D_clk_n ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {250000000} \
 ] $CLK_IN1_D_clk_n
  set CLK_IN1_D_clk_p [ create_bd_port -dir I -type clk CLK_IN1_D_clk_p ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {250000000} \
 ] $CLK_IN1_D_clk_p
  set RLED_0 [ create_bd_port -dir O RLED_0 ]
  set SCK_0 [ create_bd_port -dir O SCK_0 ]
  set SDATA_0 [ create_bd_port -dir O SDATA_0 ]
  set SS_0 [ create_bd_port -dir O -from 0 -to 0 SS_0 ]
  set clk_to_adc_DS_N [ create_bd_port -dir O -from 0 -to 0 clk_to_adc_DS_N ]
  set clk_to_adc_DS_P [ create_bd_port -dir O -from 0 -to 0 clk_to_adc_DS_P ]
  set data_from_adc_DS_N [ create_bd_port -dir I -from 15 -to 0 -type data data_from_adc_DS_N ]
  set data_from_adc_DS_P [ create_bd_port -dir I -from 15 -to 0 -type data data_from_adc_DS_P ]
  set from_adc_or_DS_N [ create_bd_port -dir I -from 0 -to 0 from_adc_or_DS_N ]
  set from_adc_or_DS_P [ create_bd_port -dir I -from 0 -to 0 from_adc_or_DS_P ]
  set rtc_1hz [ create_bd_port -dir I -type clk rtc_1hz ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {1} \
 ] $rtc_1hz
  set rtc_rst [ create_bd_port -dir O -from 0 -to 0 -type rst rtc_rst ]
  set x_from_adc_calrun_fmc [ create_bd_port -dir I x_from_adc_calrun_fmc ]
  set x_to_adc_cal_fmc [ create_bd_port -dir O x_to_adc_cal_fmc ]
  set x_to_adc_caldly_nscs_fmc [ create_bd_port -dir O x_to_adc_caldly_nscs_fmc ]
  set x_to_adc_dclk_rst_fmc [ create_bd_port -dir O x_to_adc_dclk_rst_fmc ]
  set x_to_adc_fsr_ece_fmc [ create_bd_port -dir O x_to_adc_fsr_ece_fmc ]
  set x_to_adc_led_0 [ create_bd_port -dir O x_to_adc_led_0 ]
  set x_to_adc_led_1 [ create_bd_port -dir O x_to_adc_led_1 ]
  set x_to_adc_outedge_ddr_sdata_fmc [ create_bd_port -dir O x_to_adc_outedge_ddr_sdata_fmc ]
  set x_to_adc_outv_slck_fmc [ create_bd_port -dir O x_to_adc_outv_slck_fmc ]
  set x_to_adc_pd_fmc [ create_bd_port -dir O x_to_adc_pd_fmc ]

  # Create instance: ADC_CLK_GENERATOR
  create_hier_cell_ADC_CLK_GENERATOR [current_bd_instance .] ADC_CLK_GENERATOR

  # Create instance: GFCR
  create_hier_cell_GFCR [current_bd_instance .] GFCR

  # Create instance: PL
  create_hier_cell_PL [current_bd_instance .] PL

  # Create interface connections
  connect_bd_intf_net -intf_net user_si570_sysclk_1 [get_bd_intf_ports user_si570_sysclk] [get_bd_intf_pins ADC_CLK_GENERATOR/user_si570_sysclk]

  # Create port connections
  connect_bd_net -net ADC_CLK_GENERATOR_clk_to_adc_DS_N [get_bd_ports clk_to_adc_DS_N] [get_bd_pins ADC_CLK_GENERATOR/clk_to_adc_DS_N]
  connect_bd_net -net ADC_CLK_GENERATOR_clk_to_adc_DS_P [get_bd_ports clk_to_adc_DS_P] [get_bd_pins ADC_CLK_GENERATOR/clk_to_adc_DS_P]
  connect_bd_net -net CLK_IN1_D_clk_n_1 [get_bd_ports CLK_IN1_D_clk_n] [get_bd_pins GFCR/CLK_IN1_D_clk_n]
  connect_bd_net -net CLK_IN1_D_clk_p_1 [get_bd_ports CLK_IN1_D_clk_p] [get_bd_pins GFCR/CLK_IN1_D_clk_p]
  connect_bd_net -net GFCR_PL_CLK [get_bd_pins GFCR/PL_CLK] [get_bd_pins PL/CLK]
  connect_bd_net -net GFCR_PL_rst [get_bd_ports rtc_rst] [get_bd_pins GFCR/PL_rst] [get_bd_pins PL/RST]
  connect_bd_net -net PL_RLED_0 [get_bd_ports RLED_0] [get_bd_pins PL/RLED_0]
  connect_bd_net -net PL_SCK_0 [get_bd_ports SCK_0] [get_bd_pins PL/SCK_0]
  connect_bd_net -net PL_SDATA_0 [get_bd_ports SDATA_0] [get_bd_pins PL/SDATA_0]
  connect_bd_net -net PL_SS_0 [get_bd_ports SS_0] [get_bd_pins PL/SS_0]
  connect_bd_net -net PL_x_to_adc_cal_fmc [get_bd_ports x_to_adc_cal_fmc] [get_bd_pins PL/x_to_adc_cal_fmc]
  connect_bd_net -net PL_x_to_adc_caldly_nscs_fmc [get_bd_ports x_to_adc_caldly_nscs_fmc] [get_bd_pins PL/x_to_adc_caldly_nscs_fmc]
  connect_bd_net -net PL_x_to_adc_dclk_rst_fmc [get_bd_ports x_to_adc_dclk_rst_fmc] [get_bd_pins PL/x_to_adc_dclk_rst_fmc]
  connect_bd_net -net PL_x_to_adc_fsr_ece_fmc [get_bd_ports x_to_adc_fsr_ece_fmc] [get_bd_pins PL/x_to_adc_fsr_ece_fmc]
  connect_bd_net -net PL_x_to_adc_led_0 [get_bd_ports x_to_adc_led_0] [get_bd_pins PL/x_to_adc_led_0]
  connect_bd_net -net PL_x_to_adc_led_1 [get_bd_ports x_to_adc_led_1] [get_bd_pins PL/x_to_adc_led_1]
  connect_bd_net -net PL_x_to_adc_outedge_ddr_sdata_fmc [get_bd_ports x_to_adc_outedge_ddr_sdata_fmc] [get_bd_pins PL/x_to_adc_outedge_ddr_sdata_fmc]
  connect_bd_net -net PL_x_to_adc_outv_slck_fmc [get_bd_ports x_to_adc_outv_slck_fmc] [get_bd_pins PL/x_to_adc_outv_slck_fmc]
  connect_bd_net -net PL_x_to_adc_pd_fmc [get_bd_ports x_to_adc_pd_fmc] [get_bd_pins PL/x_to_adc_pd_fmc]
  connect_bd_net -net data_from_adc_DS_N_1 [get_bd_ports data_from_adc_DS_N] [get_bd_pins PL/data_from_adc_DS_N]
  connect_bd_net -net data_from_adc_DS_P_1 [get_bd_ports data_from_adc_DS_P] [get_bd_pins PL/data_from_adc_DS_P]
  connect_bd_net -net from_adc_or_DS_N_1 [get_bd_ports from_adc_or_DS_N] [get_bd_pins PL/from_adc_or_DS_N]
  connect_bd_net -net from_adc_or_DS_P_1 [get_bd_ports from_adc_or_DS_P] [get_bd_pins PL/from_adc_or_DS_P]
  connect_bd_net -net rtc_1hz_1 [get_bd_ports rtc_1hz] [get_bd_pins PL/rtc_1hz]
  connect_bd_net -net x_from_adc_calrun_fmc_1 [get_bd_ports x_from_adc_calrun_fmc] [get_bd_pins PL/x_from_adc_calrun_fmc]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


common::send_msg_id "BD_TCL-1000" "WARNING" "This Tcl script was generated from a block design that has not been validated. It is possible that design <$design_name> may result in errors during validation."

